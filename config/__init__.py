#!/user/bin/env python3
#===============================================================================
# config
#===============================================================================

"""Create a config file"""




# Imports ======================================================================

import argparse
import os
import os.path




# Constants ====================================================================

DEVELOPMENT_CONFIG_DATA = f'''
import os
import os.path
basedir = os.path.abspath(os.path.dirname(__file__))
SECRET_KEY = os.environ.get('SECRET_KEY') or {os.urandom(16)}
PROTECTED_DIR = os.path.join(basedir, 'protected')
'''

PRODUCTION_CONFIG_DATA = f'''
import os
import os.path
basedir = os.path.abspath(os.path.dirname(__file__))
SECRET_KEY = os.environ.get('SECRET_KEY') or {os.urandom(16)}
PROTECTED_DIR = os.path.join(basedir, 'protected')
'''




# Functions ====================================================================

def parse_arguments():
    parser = argparse.ArgumentParser(
        description='write configuration file'
    )
    parser.add_argument(
        'instance',
        metavar='<path/to/instance-folder/>',
        help='path to instance folder'
    )
    parser.add_argument(
        '--production',
        action='store_true',
        help='write a production config file'
    )
    return parser.parse_args()

def main():
    args = parse_arguments()
    with open(os.path.join(args.instance, 'config.py'), 'w') as f:
        f.write(
            PRODUCTION_CONFIG_DATA if args.production
            else DEVELOPMENT_CONFIG_DATA
        )




# Execute ======================================================================

if __name__ == '__main__':
    main()
