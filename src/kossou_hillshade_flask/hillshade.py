#===============================================================================
# hillshade.py
#===============================================================================

"""Hillshade blueprint

Attributes
----------
bp : Blueprint
    blueprint object, see the flask tutorial/documentation:

    http://flask.pocoo.org/docs/1.0/tutorial/views/

    http://flask.pocoo.org/docs/1.0/blueprints/
"""


# Imports ======================================================================

import os.path

from flask import (
    Blueprint, render_template, current_app, url_for, redirect, request
)

from kossou_hillshade_flask.forms import HillshadeForm

from kossou_hillshade_base import (
    AZIMUTH, ALTITUDE, REPORT, plot_hillshade
)


# Blueprint assignment =========================================================

bp = Blueprint('strategy', __name__)

HEADER = """Sunlight Parameters
-------------------
<image src="{schematic_svg}" width="300" />
"""


# Functions ====================================================================

@bp.route('/', methods=('GET', 'POST'))
def index():
    f"""Render the hillshade index"""

    form = HillshadeForm()
    if form.validate_on_submit():
        return redirect(
            url_for(
                'hillshade.index',
                azimuth=form.azimuth.data,
                altitude=form.altitude.data
            )
        )

    azimuth = int(request.args.get('azimuth', AZIMUTH))
    altitude = int(request.args.get('altitude', ALTITUDE))
    plot_hillshade(
            os.path.join(os.path.dirname(__file__), 'static', 'n07_w006_3arc_v2.tif'),
            os.path.join(current_app.config['PROTECTED_DIR'], 'hillshade.jpg'),
            azimuth=azimuth,
            altitude=altitude,
            width=5,
            height=5
        )
    return render_template(
        'hillshade/index.html',
        header=HEADER.format(schematic_svg=url_for('static', filename='Azimuth-Altitude_schematic.svg')),
        report=REPORT.format(
            azimuth=azimuth,
            altitude=altitude,
            hillshade_jpg=url_for(
                'protected.protected',
                filename="hillshade.jpg"
            )
        ),
        form=form,
        azimuth=azimuth,
        altitude=altitude
    )
