#===============================================================================
# forms.py
#===============================================================================

"""Forms (subclasses of FlaskForm, see Flask-WTF:
http://flask-wtf.readthedocs.io/en/stable/ )
"""




# Imports  =====================================================================

from flask import request, current_app
from flask_wtf import FlaskForm
from wtforms.validators import NumberRange
from wtforms import IntegerField, SubmitField
from kossou_hillshade_base import AZIMUTH, ALTITUDE




# Classes ======================================================================

# Forms ------------------------------------------------------------------------

class HillshadeForm(FlaskForm):
    """A form for hillshade queries

    Attributes
    ----------
    azimuth : IntegerField
    altitude : IntegerField
    submit : SubmitField
    """

    azimuth = IntegerField(AZIMUTH, default=AZIMUTH,
                           validators=[NumberRange(min=0, max=360)])
    altitude = IntegerField(ALTITUDE, default=ALTITUDE,
                            validators=[NumberRange(min=0, max=90)])
    submit = SubmitField('Generate Plot')
