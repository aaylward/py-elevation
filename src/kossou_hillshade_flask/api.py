#===============================================================================
# api.py
#===============================================================================

# Imports ======================================================================

import os.path
from flask import Blueprint, request, current_app, url_for
from flask_restx import Resource, Api
from flask_restx import fields as restx_fields
from flask_marshmallow import Marshmallow
from kossou_hillshade_base import plot_hillshade, AZIMUTH, ALTITUDE


# Blueprint assignment =========================================================

bp = Blueprint('api', __name__, url_prefix='/api')


# API and Marshmallow assignments ==============================================

api = Api(bp, doc='/doc/')
ma = Marshmallow(bp)


# Constants =====================================================================

HILLSHADE_TIF = os.path.join(os.path.dirname(__file__), 'static', 'n07_w006_3arc_v2.tif')

# Models =======================================================================

hillshade_model = api.model('Hillshade', {
    'azimuth': restx_fields.Integer(default=AZIMUTH),
    'altitude': restx_fields.Integer(default=ALTITUDE),
    'width': restx_fields.Integer(default=5),
    'height': restx_fields.Integer(default=5),
    'url': restx_fields.String()
})

# Schema =======================================================================

class HillshadeSchema(ma.Schema):
    azimuth = ma.Integer(load_default=AZIMUTH, dump_default=AZIMUTH)
    altitude = ma.Integer(load_default=ALTITUDE, dump_default=ALTITUDE)
    height = ma.Integer(load_default=5, dump_default=5)
    width = ma.Integer(load_default=5, dump_default=5)
    url = ma.Str(dump_only=True)

HILLSHADE_SCHEMA = HillshadeSchema()


# Resources ====================================================================

@api.route('/hello')
class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}


@api.route('/hillshade')
class HillshadeResource(Resource):
    @api.marshal_with(hillshade_model, envelope='hillshade')
    def get(self, **kwargs):
        hillshade_url = os.path.join(current_app.config['PROTECTED_DIR'], 'hillshade.jpg')
        plot_hillshade(HILLSHADE_TIF, hillshade_url)
        return HILLSHADE_SCHEMA.dump(
            {'url': url_for(
                'protected.protected',
                filename='hillshade.jpg',
                _external=True
            )}
        )

    @api.marshal_with(hillshade_model, envelope='hillshade')
    def post(self, **kwargs):
        request_values = dict(request.values.items())
        hillshade_url = os.path.join(current_app.config['PROTECTED_DIR'], 'hillshade.jpg')
        plot_hillshade(
            HILLSHADE_TIF,
            hillshade_url,
            **HILLSHADE_SCHEMA.load(request_values)
        )
        return HILLSHADE_SCHEMA.dump(
            request_values | {'url': url_for(
                'protected.protected',
                filename='hillshade.jpg',
                _external=True
            )}
        )
